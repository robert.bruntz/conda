# Compiling code in a Conda environment

!!! tip "Use the pre-built environments"

    The [Pre-built environments](/environments/) should contain all of the
    packages needed to build most things, if packages are missing please
    [open a Request ticket](https://git.ligo.org/computing/conda/issues/new?issuable_template=Request)
    to request their addition.

To compile packages without using `conda build` you will need to build a
conda environment that contains the necessary packages. The only tricky
thing about this is installing the compilers, [read
this](https://conda.io/docs/user-guide/tasks/build-packages/compiler-tools.html).
You will need to **activate** the environment properly so as to have the
compiler tools set the necessary environment variables (`CC` etc).

!!! tip "Conda-forge `compilers` package"

    There is now a suite of conda-forge packages designed to provide a
    compiler for your architecture. See the
    [compilers](https://anaconda.org/conda-forge/compilers) package, and its
    dependencies.

## Installation prefices

Conda's environment configuration mean that dynamic linking is much more
tightly controlled than outside of an environment. This means that, for
example, building shared object libraries and installing them into
custom directories that you append to environment variables such as
`LD_LIBRARY_PATH` may not resolve links properly.

The way to avoid this is simply to always install code directly into a
conda environment. For autotools builds this would just mean using
`--prefix=${CONDA_PREFIX}` during `./configure`.

## Compiling on macOS

Compiling on macOS using conda compilers is not absolutely trivial,
there is a small set of steps that need to be followed to configure your
environment.

### Installing the macOS SDK

Conda compilers will try and compile software for the oldest compatible
version of macOS, which is 10.9 most of the time. So, you will likely
need to install the MacOSX10.9 SDK, which you can do as follows:

```shell
mkdir -p /opt
cd /opt
wget https://github.com/phracker/MacOSX-SDKs/releases/download/10.13/MacOSX10.9.sdk.tar.xz
tar -xf MacOSX10.9.sdk.tar.xz
```

Then, set the `CONDA_BUILD_SYSROOT` environment variable to match the
new SDK:

```shell
export CONDA_BUILD_SYSROOT="/opt/MacOSX10.9.sdk"
```

## Configuring the linker

In order to resolve links, especially during `./configure` steps for
autotools builds, you will need to 'activate' your build environment
with the following steps:

```shell
conda activate <env-name>
export PREFIX="${CONDA_PREFIX}"
export CONDA_BUILD="1"
conda activate <env-name>
```
