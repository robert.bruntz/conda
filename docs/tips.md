# Tips and tricks

## Clone an environment

You can clone one of the pre-built environments (or any environment,
generally) with

```shell
conda create --name <target> --clone <source>
```

e.g.:

```shell
conda create --name myigwn-py38 --clone igwn-py38
```

## Managing your own IGWN environments

You may wish to manage your own conda environments, but keep them synchronised
with the IGWN Conda Distribution package lists.
You can use the following `bash` or `powershell` functions
(add them to your login script) to create or update IGWN Conda Distribution
environments by just passing the environment name:

=== "Bash"
    ```bash
    function igwn_conda_fetch_yaml() {
        ## download the YAML definition file for an IGWN Conda Distribution environment
        local name=$1
        local _conda_platform=$(${CONDA_PYTHON_EXE} -c "from conda.base.context import context; print(context.platform)")
        curl -LOs https://computing.docs.ligo.org/conda/environments/${_conda_platform}/${name}.yaml
    }

    function igwn_conda_create() {
        ## create a local copy of an IGWN Conda Distribution environment
        local name=$1
        shift 1
        igwn_conda_fetch_yaml ${name}
        conda env create --file ${name}.yaml $@
    }

    function igwn_conda_update() {
        ## update a local copy of an IGWN Conda Distribution environment
        local name=$1
        shift 1
        igwn_conda_fetch_yaml $name
        conda env update --file ${name}.yaml $@
    }
    ```

=== "PowerShell"
    ```powershell
    function igwn_conda_fetch_yaml {
        ## download the YAML definition file for an IGWN Conda Distribution environment
        Param($Name)
        $CondaPlatform = (& $Env:CONDA_PYTHON_EXE -c "from conda.base.context import context; print(context.platform)") | Out-String -Stream
        Invoke-WebRequest -Uri https://computing.docs.ligo.org/conda/environments/${CondaPlatform}/${Name}.yaml -UseBasicParsing -OutFile "$Name.yaml"
    }

    function igwn_conda_create {
        ## create a local copy of an IGWN Conda Distribution environment
        Param($Name)
        $Output = "$Name.yaml"
        igwn_conda_fetch_yaml $Name
        conda env create --file $Output @Args
    }

    function igwn_conda_update {
        ## update a local copy of an IGWN Conda Distribution environment
        Param($Name)
        $Output = "$Name.yaml"
        igwn_conda_fetch_yaml $Name
        conda env update --file $Output @Args
    }
    ```

Then to create or update an IGWN Conda Distribution environment, simply call
the `igwn_conda_create` or `igwn_conda_update` function as appropriate:

!!! example "Creating your own IGWN Conda Distribution environment"

    ```bash
    igwn_conda_create igwn-py38
    ```

!!! example "Updating your own IGWN Conda Distribution environment"

    ```bash
    igwn_conda_update igwn-py38
    ```

## Preserve your prompt

By default, activating a conda environment will change your login
prompt. This behaviour can be disabled with

```shell
conda config --set changeps1 no
```

## Using graphics libraries on Linux {: #graphics }

Low-latency graphics libraries (e.g. implementations of OpenGL) are not
distributed as conda packages on Linux, and as such these are not included
in the IGWN Conda Distribution environments.

!!! warning "Install graphics libraries separately"

    If your workflow requires rendering graphics, you are expected to
    install these low-level libraries yourself, or ask your friendly
    systems adminstrator to do that for you.

With most systems this just requires installing `libGL.so`, but more
graphics-intensive workflows will likely require other libraries.

!!! info "macOS comes batteries-included"

    macOS (at least macOS X) seems to come with the relevant graphics
    libraries pre-loaded.

## Using LaTeX

The IGWN Conda Distribution does not provide (La)TeX. You should
install TeX using your system package manager, see below for details for
some common operating systems:

!!! warning "Should be good enough for matplotlib"

    The following examples should get you far enough to use TeX with the
    Matplotlib Python library. If you find that other packages are needed,
    please [open a Bug
    ticket](https://git.ligo.org/computing/conda/issues/new?issuable_template=Bug).

### Macports

```shell
port install \
    texlive \
    dvipng
```

### Debian/Ubuntu

```shell
apt-get install \
    dvipng \
    texlive-latex-base \
    texlive-latex-extra
```

### RHEL/Centos/Fedora

```shell
yum install \
    texlive-dvipng-bin \
    texlive-latex-bin-bin \
    texlive-type1cm \
    texlive-collection-fontsrecommended
```
