# Package change workflow

This page attempts to describe the change workflow in order to implement
change requests that have been detailed on an SCCB ticket.

## Determining version numbers and build strings

In order to add a new package, or upgrade an existing package, the new package
version and build string need to be determined.

The version number should be declared in the SCCB ticket, which can then be used
to determine the build string by using the `search.py` script:

```console
python scripts/search.py <package> <version> --yaml
```

!!! example "search.py output"
    ```console
    > python .\scripts\search.py python-framel 8.40.1 --yaml
    ```
    This will print something like this:

    ```yaml
    package:
      name: python-framel
      version: 8.40.1
      build_string: py36h785e9b2_0  # [linux and py==36]
      build_string: py37h03ebfcd_0  # [linux and py==37]
      build_string: py38h8790de6_0  # [linux and py==38]
      build_string: py36h255dfe6_0  # [osx and py==36]
      build_string: py37h10e2902_0  # [osx and py==37]
      build_string: py38h65ad66c_0  # [osx and py==38]
      build_string: py36h9902d54_0  # [win and py==36]
      build_string: py37h9dff50a_0  # [win and py==37]
      build_string: py38h377fac3_0  # [win and py==38]
    ```

!!! tip "Restricting the build"
    You can use the `--build` option to specify a build string (or wildcard)
    to restrict the results to a single build.

Once you have the new YAML content you can either add a new file for the new
package, or modify an existing file to upgrade an existing package.

## Getting packages into testing

To get packages in testing,
[open a merge request](https://git.ligo.org/computing/conda/merge_requests/new?merge_request%5Btarget_branch%5D=testing) against `testing`.
The CI will run to validate things.

Once the CI passes, **the MR can be merged immediately** in order to deploy the
new packages into the testing environments.

## Migrating packages from testing to proposed

To migrate packages from `testing` into `proposed`:

1. go back to the merge request you posted for `testing`,
   and click the _Cherry-pick_ button,
2. in the popup, under _Pick into branch_, select `master`
3. ensure that the _Start a new merge request with these changes_ radio box
   is **checked**
4. Click _Cherry-pick_.

!!! tip "Apply the right labels"
    Please then apply the _sccb::pending_ label to indicate that the SCCB
    is still considering this request.
    Please also apply any other labels that seem appropriate.

!!! warning "Wait for SCCB approval"
    Once the CI passes, **wait until the SCCB approves the request**.

As soon as the SCCB approves the request, the MR should then be merged to
deploy the new packages into the proposed environments.

## Creating a new stable release

When deployment time arrives, a new stable distribution should be created,
see [_Tagging a release_](./release.md).
