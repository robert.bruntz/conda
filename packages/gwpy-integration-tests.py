#!/usr/bin/env python

"""Run integration tests for GWpy
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

import tempfile

from matplotlib import use
use('agg')  # noqa

from gwpy.timeseries import TimeSeries


# -- test timeseries plots ----------------------

def test_timeseries_plot():
    data = TimeSeries.fetch_open_data('H1', 1126259446, 1126259478)
    qspecgram = data.q_transform(outseg=(1126259462.2, 1126259462.5))
    plot = qspecgram.plot(figsize=[8, 4])
    ax = plot.gca()
    ax.set_xscale('seconds')
    ax.set_yscale('log')
    ax.set_ylim(20, 500)
    ax.set_ylabel('Frequency [Hz]')
    ax.grid(True, axis='y', which='both')
    ax.colorbar(cmap='viridis', label='Normalized energy')
    with tempfile.TemporaryFile() as tmp:
        plot.save(tmp, format="png")
