#!/usr/bin/env python

"""Find a package for the relevant platforms
"""

import argparse
import json
import re
import subprocess
from shutil import which

PY_BUILD_REG = re.compile(r"\Apy\d\dh")
CONDA = which("conda") or "conda"


def conda_search(package, version, build, subdir, conda="conda", **kwargs):
    key = "=".join((package, version) + ((build,) if build else ()))
    out = subprocess.run(
        [
            conda,
            "search",
            key,
            "--subdir={}".format(subdir),
            "--json",
        ],
        capture_output=True,
        check=True,
        shell=conda=="conda",
    )
    return json.loads(out.stdout)[package]


def yaml_build_string(packagedata):
    build = packagedata["build"]
    subdir = packagedata["subdir"]
    selectors = []
    if subdir != "noarch":
        selectors.append(subdir.split("-", 1)[0])
    if PY_BUILD_REG.match(build):
        selectors.append("py=={}".format(build[2:4]))
    text = "  build_string: {}".format(build)
    if selectors:
        text += "  # [{}]".format(" and ".join(selectors))
    return text


parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "package",
    help="name of package to find",
)
parser.add_argument(
    "version",
    help="version of package to find",
)
parser.add_argument(
    "-b",
    "--build",
    help="build string of package to find",
)
parser.add_argument(
    "-l",
    "--skip-linux",
    action="store_true",
    help="skip linux",
)
parser.add_argument(
    "-o",
    "--skip-osx",
    action="store_true",
    help="skip osx",
)
parser.add_argument(
    "-w",
    "--skip-windows",
    action="store_true",
    help="skip windows",
)
parser.add_argument(
    "-c",
    "--conda",
    default=CONDA,
    help="path of conda executable",
)
parser.add_argument(
    "-y",
    "--yaml",
    action="store_true",
    default=False,
    help="print in YAML format",
)

args = parser.parse_args()

name = args.package
version = args.version
build = args.build
conda = args.conda

# -- execute searches

if args.yaml:
    print(
        "package:\n"
        "  name: {}\n"
        "  version: {}".format(name, version)
    )

for skip, subdir in [
        (args.skip_linux, "linux-64"),
        (args.skip_osx, "osx-64"),
        (args.skip_windows, "win-64"),
]:
    if skip:
        continue
    for package in conda_search(name, version, build, subdir, conda=conda):
        if args.yaml:
            print(yaml_build_string(package))
        else:
            print(
                "{0[subdir]:9s} {0[name]} {0[version]} {0[build]}".format(
                    package,
                ),
            )
    if package["subdir"] == "noarch":  # only search once for noarch
        break

